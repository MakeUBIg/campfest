

<?php
if(isset($_POST['Send']))
{
$first_name=$_POST["first_name"];
$last_name=$_POST["last_name"];
$age=$_POST["age"];
$email=$_POST["email"];
$phone=$_POST["phone"];
$gender=$_POST["gender"];
$address=$_POST["address"];
$volunteer_categaties=$_POST["volunteer_categaries"];
$speak_english=$_POST["speak_english"];

}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Volunteer with Campfest</title>
  <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
  
  <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
<link rel='stylesheet prefetch' href='http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css'>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container">

<form class="well form-horizontal" action="/Sendmail.php" method="post"  id="contact_form">
<fieldset>
<!-- Form Name -->
<legend>Volunteer with CampFest</legend>

<div class="form-group">
  <label class="col-md-4 control-label">First Name</label>  
  <div class="col-md-4 inputGroupContainer">
  <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input  name="first_name" placeholder="First Name" class="form-control"  type="text">
    </div>
  </div>
</div>

<!-- Text input-->

<div class="form-group">
  <label class="col-md-4 control-label" >Last Name</label> 
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input name="last_name" placeholder="Last Name" class="form-control"  type="text">
    </div>
  </div>
</div>



<div class="form-group">
  <label class="col-md-4 control-label" >Age</label> 
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input name="age" placeholder="Age" class="form-control"  type="text">
    </div>
  </div>
</div>






<!-- Text input-->
       <div class="form-group">
  <label class="col-md-4 control-label">E-Mail</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
  <input name="email" placeholder="E-Mail Address" class="form-control"  type="text">
    </div>
  </div>
</div>


<!-- Text input-->
       
<div class="form-group">
  <label class="col-md-4 control-label">Phone </label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
  <input name="phone" placeholder="(845)555-1212" class="form-control" type="text">
    </div>
  </div>
</div>

<div class="form-group"> 
  <label class="col-md-4 control-label">Gender</label>
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <select name="gender" class="form-control selectpicker" name="gender" >
<option value=" ">Please select your gender</option>
<option value="male">Male</option>
<option value="female">Female</option>

</select>
  </div>
</div>
</div>
      
<div class="form-group">
  <label class="col-md-4 control-label">Address</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
  <input name="address" placeholder="Address" class="form-control" type="text">
    </div>
  </div>
</div>

<!-- Text input-->
 


<!-- Select Basic -->
   
<div class="form-group">
                        <label class="col-md-4 control-label">Volunteer Categaries</label>
                        <div class="col-md-4">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="volunteer_categaries" value="yes" /> <b>Site Construction / Set up</b>
                                           (Build the festival site, including decoration, stage setting)
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="volunteer_categaries" value="no" /><b>Security</b>
                                    (Provide a safe and healthy environment at the festival site and make sure no littering in festival arena)

                                </label>
                           </div>
                           <div class="radio">
                                <label>
                                    <input type="radio" name="volunteer_categaries" value="no" /><b>Hospitality</b>
                                         ( Generous and friendly help to the festival attendees)

                                </label>
                           </div>
                           <div class="radio">
                                <label>
                                    <input type="radio" name="volunteer_categaries" value="no" /><b>First Aid</b>
                             (Basic first aid kit with medications for common illness such as headaches)

                                </label>
                           </div>
                           <div class="radio">
                                <label>
                                    <input type="radio" name="volunteer_categaries" value="no" /><b>Artists</b> (Musicians,
                                                                                          Stage Performers ,
                                                                                 Photographers/Videographers,
                                                                                    Street Performers,
                                                                                    Tattoo Artists)                                           
                                </label>
                           </div>
                        </div>
                    </div>


<!-- Text input-->


<!-- Text input-->

 
<!-- radio checks -->
 <div class="form-group">
                        <label class="col-md-4 control-label">Do you speak English?</label>
                        <div class="col-md-4">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="speak_english" value="yes" /> Yes
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="speak_english" value="no" /> No
                                </label>
                            </div>
                        </div>
                    </div>

<!-- Text area -->
 

<!-- Success message -->
<div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label"></label>
  <div class="col-md-4">
    <button type="submit" class="btn btn-warning" name="Send" >Send <span class="glyphicon glyphicon-send"></span></button>
  </div>
</div>

</fieldset>
</form>
</div>
    </div><!-- /.container -->
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>

    <script src="js/index.js"></script>

</body>
</html>
