
<!DOCTYPE HTML>
<html> 
<head>
<title>CampFest | July 2017</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<link rel="icon" type="image/png" href="/favicon.png" />
<meta property="og:title" content="CampFest | December 2016" />
<meta property="og:url" content="http://campfest.in" />

<!-- css files -->
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="/css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- /css files -->
<!-- font files -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
<link href='fonts/playlist.otf' rel='stylesheet' type='text/css'>
<link href='fonts/playlist.woff' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!-- /font files -->
<!-- js files -->
<script src="js/modernizr.custom.js"></script>
<link rel="stylesheet" type="text/css" href="css/slick.css">
<!-- /js files -->
<style>.slideanim {
	     visibility: visible; 
	 }
	 section.our-volunteer .form-control {
	 	background-color:rgba(204, 119, 119, 0.48);
	 }
	     </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44367293-2', 'auto');
  ga('send', 'pageview');

</script>
<?php echo '<script>alert("The volunteer Registration is now closed! \n Stay close for more!"); window.location.assign("http://campfest.in"); </script>';
return false;
?>
<section class="our-volunteer slideanim" id="volunteer">
	<h3 class="text-center slideanim logotext">Volunteer With CampFest</h3>
	<p class="text-center slideanim">Our doors are always open to like-minded people who wish to experience the festival as a volunteer.<br /> In exchange for your help we'll sort you out with a festival ticket, stay and 3 meals a day.<br />Positions such as <strong>decor crew, sales heroes and artist are available.</strong><br />
	If you have relevant experience or think you have something to offer, we would love to hear from you.<br /> Please drop us a line with a brief description on your experience and what you would like to do on <strong><a href="mailto:himalayanrootscottages@gmail.com" style="text-decoration:none;color:#fff">himalayanrootscottages@gmail.com
	
</a></strong>
</p>
	<div class="container">
		<div class="row">
		<div class="col-lg-2"></div>
			<div class="col-lg-8">
				<form name="frm" id="volunteerForm" >
					<div class="row">
						<div class="form-group col-md-12 slideanim">
						
							<input type="text" id="name" name="first_name" class="form-control user-name" placeholder="Name" required/>
						</div>
						<div class="form-group col-lg-12 slideanim">
							<input type="text" id="name" name="age" class="form-control user-name" placeholder=" Age" required/>
						</div>
						<div class="form-group col-lg-12 slideanim">
							<input type="email" id="email" name="email" class="form-control mail" placeholder=" Email" required/>
						</div>
						<div class="form-group col-lg-12 slideanim">
							<input type="text" name="phone" id="phone" class="form-control pno" placeholder=" Phone Number" required/>
						</div>
						<div class="form-group col-lg-12 slideanim">
							<input type="text" id="email" name="address" class="form-control mail" placeholder=" Address" required/>
						</div>	

						<div class="form-group col-lg-12 slideanim">
							<select class="form-control slideanim"  style="height: 3em" name="gender" required/>
							<option value="">Gender</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>	
                            <option value="others">Others</option>
							</select>
						</div>
						<div class="form-group col-lg-12 slideanim">
							<select class="form-control slideanim"  style="height: 3em" name="speak" required/>
							<option value="">Do You Speak English ?</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>	
                    		</select>
						</div>
						<div class="form-group col-lg-12 slideanim">
							<select class="form-control slideanim"  style="height: 3em" name="volunteer" required/>
							<option value="">Volunteer Categories</option>
                            <option value="Site Construction">Site Construction / Set up
                                           (Build the festival site, including decoration, stage setting)</option>
                            <option value="Security">Security
                                    (Provide a safe and healthy environment at the festival site and make sure no littering in festival arena)</option>	
                            <option value="Hospitality">Hospitality
                                         ( Generous and friendly help to the festival attendees)
</option>
                            <option value="First Aid">First Aid
                             (Basic first aid kit with medications for common illness such as headaches)</option>
                            <option value="Artists">Artists (Musicians,
                                                                             Stage Performers ,
                                                                                 Photographers/Videographers,
                                                                                    Street Performers,
                                                                                    Tattoo Artists)  </option>
                            
                    		</select>
						</div>
						             
              		<div class="col-lg-2"></div>
						<div class="clearfix"></div>
						
						<div class="form-group col-lg-12 slideanim">
							<input type="submit" id="submitForm" value="Submit" class="btn-outline1" >
						</div>

					</div>
				</form>
			</div>	
		</div>
	</div>
</section>
<!-- /Contact Section -->
<!-- Footer Section -->
<section class="footer">
	<h2 class="text-center">THANKS FOR VISITING US</h2>
	<hr>
	<div class="container">
		<div class="row">
			<div class="col-md-4 footer-left">
				<h4>Contact Information</h4>
				<div class="contact-info">
					<div class="address">	
						<i class="glyphicon glyphicon-globe"></i>
						<p class="p3">A-05/26</p>
						<p class="p4">Sector-18, Rohini, Delhi</p>
					</div>
					<div class="phone">
						<i class="glyphicon glyphicon-phone-alt"></i>
						<p class="p3">+91-8744034432</p>
						<p class="p4">+91-9953547408</p>
					</div>
					<div class="email-info">
						<i class="glyphicon glyphicon-envelope"></i>
						<p class="p3"><a href="mailto: himalayanrootscottages@gmail.com"> himalayanrootscottages@gmail.com</a></p> 
					</div>
				</div>
			</div><!-- col -->
			<div class="col-md-4 footer-center">
				<h4>Newsletter</h4>
				<p>Register to our newsletter and be updated with the latests information regarding our services, offers and much more.</p>
			</div><!-- col -->
			<div class="col-md-4 footer-right">
				<h4>Support Us</h4>
				<p>For Sponsorship/ Tie ups / Branding opportunities, call us at the give numbers</p>
				<ul class="social-icons2">
					<li><a href="https://www.facebook.com/himalayanrootscottages"><i class="fa fa-facebook"></i></a></li>
				</ul>
			</div><!-- col -->
		</div><!-- row -->
	</div><!-- container -->
	<hr>
	<div class="copyright">
		<p>© <?= date('Y',time());?> CampFest. All Rights Reserved | Design by <a href="http://MakeUBig.com" target="_blank">MakeUBig</a></p>
	</div>
</section>


<!-- js files -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/SmoothScroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<!-- js for gallery -->
<script src="js/darkbox.js"></script>
<script src="js/coundown-timer.js"></script>
<!-- /js for gallery -->
<!-- js for back to top -->
<script src="js/main.js"></script>
<script src="js/slick.min.js"></script>
<!-- /js for back to top -->
<!-- js for nav-smooth scroll -->
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

  // Store hash
  var hash = this.hash;

  // Using jQuery's animate() method to add smooth page scroll
  // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
  $('html, body').animate({
    scrollTop: $(hash).offset().top
  }, 900, function(){

    // Add hash (#) to URL when done scrolling (default click behavior)
    window.location.hash = hash;
    });
  });
})
</script>
<!-- /js for nav-smooth scroll -->
<!-- js for slide animations -->
<script>
$(window).scroll(function() {
  $(".slideanim").each(function(){
    var pos = $(this).offset().top;

    var winTop = $(window).scrollTop();
    if (pos < winTop + 600) {
      $(this).addClass("slide");
    }
  });
});
</script>
<!-- /js for slide animations -->
<!-- /js files -->
</body>
</html>
