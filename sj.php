
<!DOCTYPE HTML>
<html>
<head>
<title>CampFest website by MakeUBig</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="CampFest Travel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- css files -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- /css files -->
<!-- font files -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
<link href='fonts/playlist.otf' rel='stylesheet' type='text/css'>
<link href='fonts/playlist.woff' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!-- /font files -->
<!-- js files -->
<script src="js/modernizr.custom.js"></script>
<link rel="stylesheet" type="text/css" href="css/slick.css">
<!-- /js files -->
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<div class="navbar-wrapper">
    <div class="container">
		<nav class="navbar navbar-inverse navbar-static-top cl-effect-20">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand logotext" style="left:10px" href="index.html"><img width="175" src="images/cf_logo.png"></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse navbar-right">
					<ul class="nav navbar-nav">
						<li id="home"><a href="#"><span data-hover="Home">Home</span></a></li>
						<li><a href="#about"><span data-hover="About">About</span></a></li>
						<li><a href="#performers"><span data-hover="Performers">Performers</span></a></li>
						<li><a href="#service"><span data-hover="Activities">Activities</span></a></li>
						<li><a href="#events"><span data-hover="Accomodation">Accomodation</span></a></li>
						<li><a href="#gallery"><span data-hover="Gallery">Gallery</span></a></li>
						<li><a href="#contact"><span data-hover="Contact">Contact</span></a></li>
					</ul>
				</div>
			</div>
        </nav>
    </div>
</div>
<!-- Banner Section -->
<!-- Carousel
    ================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
			<img class="first-slide" src="images/bg.jpg" alt="First slide">
        </div>
        <div class="item">
			<img class="first-slide" src="images/bg1.jpg" alt="First slide">
        </div>
        <div class="item">
			<img class="second-slide" src="images/bg2.jpg" alt="Second slide">
        </div>
        <div class="item">
			<img class="third-slide" src="images/bg3.jpg" alt="Third slide">
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div><!-- /.carousel -->
<!-- /Banner Section -->

<section class="about-us" id="about">
	<h3 class="text-center slideanim logotext">About CampFest</h3>
	<p class="text-center slideanim">Nature | Music | Art | Adventure</p>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<img src="images/bg2.jpg" alt="about" class="img-responsive slideanim">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="about-info">
					<p class="abt slideanim">CampFest is the celebration of life in its own form. Different people from different parts of the world travel to an offbeat destination in the Himalayas, called Kanatal in Uttarakhand. They are all different, yet they all get united in the vibes of surreal mountains, whispering pines, open skies & chilled nights. To add a catalysing effect to this, the musicians takes the charge. We promise you that you will forget everything for the while. From adventure activities & forest trails to Art, culture, Music & Dance. CampFest has so many beautiful things to do for everyone of all ages. And Yes, the Food is so amazing over there that you'll crave for more even when you're full.</p>
					<p id="sticky-anchor" class="abt slideanim col-md-6"><button  id="sticky" type="button" class="btn btn-info btn-lg buy-ticket sticky pull-right" data-toggle="modal" data-target="#myModal">Yes I am Intested</button></p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Testimonials -->
<section class="our-perf slideanim" id="performers">
	<h3 class="text-center slideanim logotext">Our Star Performers</h3>
	<p class="text-center slideanim">You can never get Enough of them</p>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<div class="event-info">
				<img class="img-responsive" src="images/performers/boom_shankar.jpg">
					<h4 class="text-center slideanim">Boom Shankar Project</h4>
					<h5 class="text-center slideanim artist-info">World Fusion</h5>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="event-info">
				<img class="img-responsive" src="images/performers/rishabh.jpg">
					<h4 class="text-center slideanim">Rishabh Shankar</h4>
					<h5 class="text-center slideanim artist-info">Live Guitar</h5>
				</div>
			</div>	
			<div class="col-lg-4 col-md-4">
				<div class="event-info">
				<img class="img-responsive" src="images/performers/ashwani.jpg">
					<h4 class="text-center slideanim">Ashwani Basoya</h4>
					<h5 class="text-center slideanim artist-info">Singer Boom Shankar Project</h5>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<div class="event-info">
				<img class="img-responsive" src="images/performers/shankar.jpg">
					<h4 class="text-center slideanim">Shankar Chaterjee</h4>
					<h5 class="text-center slideanim artist-info">Purcussions</h5>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="event-info">
				<img class="img-responsive" src="images/performers/chandan.jpg">
					<h4 class="text-center slideanim">Chandan Bisht</h4>
					<h5 class="text-center slideanim artist-info">Didgeridoo</h5>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="event-info">
				<img class="img-responsive" src="images/performers/madhu.jpg">
					<h4 class="text-center slideanim">Madhusudhan</h4>
					<h5 class="text-center slideanim artist-info">Baul Music</h5>
				</div>
			</div>
		</div>
		<div class="row">	
			<div class="col-lg-4 col-md-4">
				<div class="event-info">
					<img class="img-responsive" src="images/performers/ankit.jpg">
					<h4 class="text-center slideanim">Ankit Yadav</h4>
					<h5 class="text-center slideanim artist-info">Electronic Dance Music</h5>
				</div>
			</div>		
			<div class="col-lg-4 col-md-4">
				<div class="event-info">
					<img class="img-responsive" src="images/performers/abhi.jpg">
					<h4 class="text-center slideanim">Abhishek Mehra</h4>
					<h5 class="text-center slideanim artist-info">Acoustic Guitar</h5>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="event-info">
					<img class="img-responsive" src="images/performers/paddy.jpg">
					<h4 class="text-center slideanim">Parvinder Singh</h4>
					<h5 class="text-center slideanim artist-info">StandUp Comedy</h5>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Testimonials -->
<!-- Services Section -->
<section class="our-services slideanim" id="service">
	<h3 class="text-center slideanim logotext">Activities & features</h3>
	<p class="text-center slideanim"> Here's the list of 'Things to do' at CampFest</p>
	<div id="features">
		<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3">
				<div class="event-info">
					<img class="img-responsive" src="images/activities/outdoor_cinema.jpg">
					<span class="text-center imgDescription"><p>Outdoor Cinema</p></span>
				</div>
			</div>
			<div class="col-lg-3 col-md-3">
				<div class="event-info">
				<img class="img-responsive" src="images/activities/forest_treck.jpg">
					<span class="text-center imgDescription"><p>Trek to Kaudia Forest</p></span>
				</div>
			</div>	
			<div class="col-lg-3 col-md-3">
				<div class="event-info">
				<img class="img-responsive" src="images/activities/Jamming.jpg">
					<span class="text-center imgDescription"><p>Music Jam Sessions</p></span>
				</div>
			</div>
			<div class="col-lg-3 col-md-3">
				<div class="event-info">
				<img class="img-responsive" src="images/activities/zipline.jpg">
					<span class="text-center imgDescription"><p>Zipline</p></span>
				</div>
			</div>	
		</div>
		<div class="row" style="margin-top:20px">
			<div class="col-lg-3 col-md-3">
				<div class="event-info">
					<img class="img-responsive" src="images/activities/sky_rope_walk.jpg">
					<span class="text-center imgDescription"><p>Sky Rope</p></span>
				</div>
			</div>
			<div class="col-lg-3 col-md-3">
				<div class="event-info">
				<img class="img-responsive" src="images/activities/sky_bridge.jpg">
					<span class="text-center imgDescription"><p>Sky Bridge Walk</p></span>
				</div>
			</div>	
			<div class="col-lg-3 col-md-3">
				<div class="event-info">
				<img class="img-responsive" src="images/activities/valley_crossing.jpg">
					<span class="text-center imgDescription"><p>Valley Crossing</p></span>
				</div>
			</div>
			<div class="col-lg-3 col-md-3">
				<div class="event-info">
				<img class="img-responsive" src="images/activities/live_performance.jpg">
					<span class="text-center imgDescription"><p> Live Music Performances</p></span>
				</div>
			</div>	
		</div>
		<div class="row" style="margin-top:20px">
			<div class="col-lg-3 col-md-3">
				<div class="event-info">
					<img class="img-responsive" src="images/activities/hammoking.jpg">
					<span class="text-center imgDescription"><p>Hammocking</p></span>
				</div>
			</div>
			<div class="col-lg-3 col-md-3">
				<div class="event-info">
				<img class="img-responsive" src="images/activities/photobooth.jpg">
					<span class="text-center imgDescription"><p>Photo Booth</p></span>
				</div>
			</div>	
			<div class="col-lg-3 col-md-3">
				<div class="event-info">
				<img class="img-responsive" src="images/activities/suc.png">
					<span class="text-center imgDescription"><p>Stand Up Comedy</p></span>
				</div>
			</div>
			<div class="col-lg-3 col-md-3">
				<div class="event-info">
				<img class="img-responsive" src="images/activities/fun.jpg">
					<span class="text-center imgDescription"><p>Electronic Dance Music</p></span>
				</div>
			</div>	
		</div>
	</div>
	</div><!--/ #features -->
</section>
<!-- /Services Section -->
<!-- Events -->
<section class="our-events slideanim" id="events">
	<h3 class="text-center slideanim logotext">Accommodations</h3>
	<p class="text-center slideanim"> And you can choose your stay from these options</p>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<div class="event-info">
					<img class="img-responsive" src="images/cottage/camp.jpg">
					<h4 class="text-center slideanim">Triple Sharing Tent</h4>
					<p class="eve slideanim"> Dome/ Alpine Tents on triple sharing basis with neat & clean floor mattresses & quilts. Common Washrooms & Toilets.<br /><br />
					<strong>Note</strong>: If you want Tent on twin sharing basis, contact the organizer</p>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="event-info">
				<img class="img-responsive" src="images/cottage/cottage.jpg">
					<h4 class="text-center slideanim">Six Sharing Friends cottage</h4>
					<p class="eve slideanim"> Bamboo cottages on 6 sharing basis with neat & clean floor mattresses & quilts. Attached toilets & washrooms & electricity enabled.<br /><br />
					<strong>Note</strong>: Cottages on twin/triple sharing are SOLD OUT</p>
				</div>
			</div>	
		</div>
	</div>
</section>
<!-- /Events -->


<!-- Next Event Section -->

<section class="next-event" id="next">
	<h3 class="text-center slideanim logotext">Countdown</h3>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4">
				<img src="images/clock.png" alt="about" class="img-responsive slideanim">
			</div>
			<span style="color:#fff;margin:10px;"></span>
			<div class="col-lg-8 col-md-8 col-sm-8">
				<ul id="countdown">
					<li>					
						<span class="days time-font">00</span>
						<strong>days</strong>
					</li>
					<li>
						<span class="hours time-font">00</span>
						<strong>hours</strong>
					</li>
					<li>
						<span class="minutes time-font">00</span>
						<strong>minutes</strong>
					</li>
					<li>
						<span class="seconds time-font">00</span>
						<strong>seconds</strong>
					</li>				
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- Gallery Section -->
<section class="our-sponsors" id="sponsors">
	<h3 class="text-center slideanim logotext">Our Sponsors</h3>
	<p class="text-center slideanim">Partners & associates</p>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<img src="images/sponsors/hr.png" class="img-responsive slideanim">
			</div>
			<div class="col-md-8">
				<h3 style="font-size:30px;color:#fff">Venue Partner</h3>
				<p style="font-size:25px;color:#fff">Himalayan Roots is a beautiful resort created in the Himalayan mountains of Kanatal, Uttarkhand. A perfect place for those who loves offbeat destinations.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
        		<img src="images/sponsors/makeubig.jpg" class="img-responsive slideanim">
        	</div>
			<div class="col-md-8">
				<h3 style="font-size:30px;color:#fff">Technology Partner</h3>
				<p style="font-size:25px;color:#fff">MakeUBig is a digital marketing and online branding service provider for SME's and Startups aimed at attaining global exposure for its customers & associate partners.<br /><br />
				Visit <strong><a href="http://makeubig.com" style="text-decoration:none;color:#fff">MakeUBig</a></strong> to know More.. 
				</p>
			</div>
		</div>
		
	</div>
</section>
<!-- Gallery Section -->
<section class="our-gallery" id="gallery">
	<h3 class="text-center slideanim logotext">Gallery</h3>
	<p class="text-center slideanim">Beautiful moments creates Beautiful memories, check them out here</p>
	<div class="container">
	<?php for($i=1;$i<22;$i++){?>
		<img src="images/gallery/thumbnail/<?=$i.'.jpg';?>" data-darkbox="images/gallery/<?=$i.'.jpg';?>" data-darkbox-description="<b>Kanhatal</b><br>A trip by Himalyan Roots" class="img-responsive slideanim">
		<?php }?>
	</div>
</section>
<!-- /Gallery Section -->
<!-- About Section -->

<!-- /About Section -->

<!-- Contact Section -->
<section class="our-video slideanim" id="peektocampfest">
	<h3 class="text-center slideanim logotext">A peek into CampFest</h3>
	<p class="text-center slideanim">Checkout the teaser of CampFest Sep2016 Aftermovie</p>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<form role="form">
					<div class="row text-center">
					<div class="form-group col-lg-2 slideanim">
						</div>
						<div class="form-group col-lg-8 slideanim">
							<iframe height="315" class="col-md-12" src="https://www.youtube.com/embed/jIQnIBVLC30" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="form-group col-lg-2 slideanim">
						</div>
					</div>
				</form>
			</div>	
		</div>
	</div>
</section>
<!-- /Contact Section -->

<!-- Contact Section -->
<section class="our-contacts slideanim" id="contact">
	<h3 class="text-center slideanim logotext">Contact Us</h3>
	<p class="text-center slideanim">Our doors are always open to like-minded people who wish to experience the festival as a volunteer.<br /> In exchange for your help we'll sort you out with a festival ticket, stay and 3 meals a day.<br />Positions such as <strong>decor crew, sales heroes and artist are available.</strong><br />
	If you have relevant experience or think you have something to offer, we would love to hear from you.<br /> Please drop us a line with a brief description on your experience and what you would like to do on <strong><a href="mailto:himalayanroortscottages@gmail.com" style="text-decoration:none;color:#fff">himalayanroortscottages@gmail.com
	
</a></strong>
</p>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<form role="form">
					<div class="row">
						<div class="form-group col-lg-4 slideanim">
							<input type="text" id="name" class="form-control user-name" placeholder="Your Name" required/>
						</div>
						<div class="form-group col-lg-4 slideanim">
							<input type="email" id="email" class="form-control mail" placeholder="Your Email" required/>
						</div>
						<div class="form-group col-lg-4 slideanim">
							<input type="tel" id="phone" class="form-control pno" placeholder="Your Phone Number" required/>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-lg-12 slideanim">
							<textarea name="msg" class="form-control" rows="6" placeholder="Your Message" required/></textarea>
						</div>
						<div class="form-group col-lg-12 slideanim">
							<input type="button"id="submitForm" value="Submit" class="btn-outline1">
						</div>
					</div>
				</form>
			</div>	
		</div>
	</div>
</section>
<!-- /Contact Section -->
<!-- Footer Section -->
<section class="footer">
	<h2 class="text-center">THANKS FOR VISITING US</h2>
	<hr>
	<div class="container">
		<div class="row">
			<div class="col-md-4 footer-left">
				<h4>Contact Information</h4>
				<div class="contact-info">
					<div class="address">	
						<i class="glyphicon glyphicon-globe"></i>
						<p class="p3">A-05/26</p>
						<p class="p4">Sector-18, Rohini, Delhi</p>
					</div>
					<div class="phone">
						<i class="glyphicon glyphicon-phone-alt"></i>
						<p class="p3">+91-8744034432</p>
						<p class="p4">+91-9953547408</p>
					</div>
					<div class="email-info">
						<i class="glyphicon glyphicon-envelope"></i>
						<p class="p3"><a href="mailto: himalayanroortscottages@gmail.com"> himalayanroortscottages@gmail.com</a></p> 
					</div>
				</div>
			</div><!-- col -->
			<div class="col-md-4 footer-center">
				<h4>Newsletter</h4>
				<p>Register to our newsletter and be updated with the latests information regarding our services, offers and much more.</p>
			</div><!-- col -->
			<div class="col-md-4 footer-right">
				<h4>Support Us</h4>
				<p>For Sponsorship/ Tie ups / Branding opportunities, call us at the give numbers</p>
				<ul class="social-icons2">
					<li><a href="https://www.facebook.com/himalayanrootscottages"><i class="fa fa-facebook"></i></a></li>
				</ul>
			</div><!-- col -->
		</div><!-- row -->
	</div><!-- container -->
	<hr>
	<div class="copyright">
		<p>© <?= date('Y',time());?> CampFest. All Rights Reserved | Design by <a href="http://MakeUBig.com" target="_blank">MakeUBig</a></p>
	</div>
</section>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <iframe src="https://in.explara.com/widget-new/campfest" frameborder="0" style="width:100%;" class="col-md-10" height="900px"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- /Footer Section -->
<!-- Back To Top -->
<a href="#0" class="cd-top">Top</a>
<!-- /Back To Top -->

<!-- js files -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/SmoothScroll.min.js"></script>
<!-- js for gallery -->
<script src="js/darkbox.js"></script>
<script src="js/coundown-timer.js"></script>
<!-- /js for gallery -->
<!-- js for back to top -->
<script src="js/main.js"></script>
<script src="js/slick.min.js"></script>
<!-- /js for back to top -->
<!-- js for nav-smooth scroll -->
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

  // Store hash
  var hash = this.hash;

  // Using jQuery's animate() method to add smooth page scroll
  // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
  $('html, body').animate({
    scrollTop: $(hash).offset().top
  }, 900, function(){

    // Add hash (#) to URL when done scrolling (default click behavior)
    window.location.hash = hash;
    });
  });
})
</script>
<!-- /js for nav-smooth scroll -->
<!-- js for slide animations -->
<script>
$(window).scroll(function() {
  $(".slideanim").each(function(){
    var pos = $(this).offset().top;

    var winTop = $(window).scrollTop();
    if (pos < winTop + 600) {
      $(this).addClass("slide");
    }
  });
});
</script>
<!-- /js for slide animations -->
<!-- /js files -->
</body>
</html>
