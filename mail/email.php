<!DOCTYPE html>
<html>
<title>Campfest Mail</title>
<body style="width:700;
margin:auto;
padding:10px;
background-color: #fff;">
<p style="text-align: justify; font-family: Verdana, Geneva, sans-serif;">
Hi {{name}}<br/>
<br/>
Thanks for showing interest in CampFest Music, Adventure, Stand Up & More. </p>
<h2 style="color: red;"><i>Dates: 14-17 July'17</i>
</h2>
<img src="http://campfest.in/images/mail/pic3.jpg" width="700px" height="300px">
<h2 style="color: red;">
<i>FEATURING</i>
</h2>
<p style="font-family: Verdana, Geneva, sans-serif;">
Electronic Stage <br/>
Live Stage <br/>
Stand up Acts<br/> 
Adventure <br/>
Camping Arena <br/>
Chill out zones <br/>
Outdoor movie screening <br/>
Road Trip<br/>
</p>
<img src="http://campfest.in/images/mail/pic8.png" width="500px"><br/>
<h2 style="color: red;">
<i>PACKAGES</i>
</h2>
<img src="http://campfest.in/images/mail/pic10.png" width="500px" height="600px">


​<h2 style="color: red;"><i>Guidelines:</i></h2>
<p style="text-align: justify; font-family: Verdana, Geneva, sans-serif;">
1. Event Tickets are non-refundable, you can transfer the ticket if needed
<br />
2. Don't bring any valuable item at CampFest, the venue will not accept responsibility for lost or stolen goods.<br />
3. Fireworks, smoke canisters, knives and imitation weapons are not permitted in the Arena. Any person in possession of such items will be refused entry.<br />
4. The timings mentioned in the itinerary might get changed a bit in-case of unforeseen circumstances, we request all to kindly cooperate in such cases.<br />
5. Snacks/ Food during the travel is not included in the package
</p>
<h2 style="color: red; text-align: justify;"><i>FAQ'S</i></h2>
<p style="font-family: Verdana, Geneva, sans-serif;">
	1. What is the weather like, is it hot, cold, or both? : Cold<br />
	<br />
	2. Meeting Point and Time :-
Delhi: 14th July, 10 pm at Netaji Subash Palace (Near Pitampura, Delhi)      Dehradun : 15th July, 11 am at Dehradun Airport<br />
<br />
    3. When are we getting back?: 
Delhi : 17th July, 10 pm at Netaji Subash Palace (Near Pitampura,
 Delhi)     Dehradun : 17th July, 2pm at Dehradun Airport<br />
 <br />
 4. I'm a single traveler, Can I join the group? : Yes, you will be sharing a cottage/tent with another traveler of the same sex<br />
<br />

5. Is there electricity in cottages/tents/Rooms? : Cottages : Yes , Tents : No , Rooms : Yes<br />
<br />

6. Are their flush-able toilets and showers? : Yes<br />
<br />

7. Will there be any charging points for mobile and other accessories : Cottages : Yes, Tents : No, Common Point : Yes<br />
<br />

8. Will food be sold at the festival? : Meals are included in the package<br />
<br />

9. Will there will be any stall to buy additional FnB item? : Yes<br />
<br />

10. Is there parking space? : Yes<br />
<br />

11. Are day passes available? : No<br />
<br />

12. Can I bring my pet? : Yes!!<br />
<br />

13. Mode of Transportation : A/C Coach<br />
<br />

14. Are there ATMs available if I run out of cash? : Yes, nearest ATM is 15km from Venue<br />
<br />
15. Will there be bonfire in the evening? Yes
</p>

<h1 style="color: red;"><i>BUY TICKETS  <a href="http://campfest.in/">CAMPFEST.IN</a></i></h1>

<h3>With Love,</h3>
<h3>Thanks & Regards</h3>
<img src="http://campfest.in/images/mail/pic4.jpg" width="700px" height="400px"><br/>
</body>
</html>