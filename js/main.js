jQuery(document).ready(function($){
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});

	$("#interested-form").validate({
		// debug: true,
		errorClass: "has-error",
		validClass: "has-success",

		rules: {

			fullname: {
				required: true,
				minlength: 2
			},
			email: {
				required: true,
				email: true
			},
			msg: {
				required: true,
				minlength:10
			},
			contact: {
				required: true,
        number: true
			},
			grpsize: {
				required: true,
			},
			messages: {
				fullname: "Please enter your fullname",
    		email: "Please enter a valid email address",
    		msg: "please enter only 10 digit",
    		contact	: "please enter a valid phone number",
    		grpsize	: "please enter a valid number"
		}
	},
	submitHandler : function(form) {
    var form = $('#interested-form').serialize();
		$.ajax({
         type:'POST',
         url:"sendmail.php",
         data:form,
        success: function(result)
        {
        	$('#interested-form').html("<center><h3>Thank you for Contacting Us.</h3><br /><h5>We will Soon get in touch with you!</h5></center>");
        }
    	});
}
});

	$('#volunteerForm').validate({	
		errorClass: "has-error",
		validClass: "has-success",

		rules: {

			first_name: {
				required: true,
				minlength: 2
			},
			last_name: {
				required: true,
				minlength: 2
			},
			email: {
				required: true,
				email: true
			},
			age: {
				required: true,
				digits: true
			},
			phone: {
				required: true,
        		number: true
			},
			gender: {
				required: true,
			},
			volunteer: {
				required: true,
			},
			speak: {
				required: true,
			},
			messages: {
			first_name: "Please enter your first name",
			last_name: "Please enter your last name",
    		email: "Please enter a valid email address",
    		phone	: "please enter a valid phone number",
    		volunteer	: "please enter a valid category"
		}
	},
	submitHandler : function(form) {console.log('volunteerForm');
    var form = $('#volunteerForm').serialize();
		$.ajax({
         type:'POST',
         url:"sendmail.php",
         data:form,
        success: function(result)
        {
        	alert('Thanks for your interest in volunteering with us! We will call you soon');
            window.location.replace('http://campfest.in');
        }
    	});
	}
});


$("#contact-form").validate({
		// debug: true,
		errorClass: "has-error",
		validClass: "has-success",

		rules: {

			fullname: {
				required: true,
				minlength: 2
			},
			email: {
				required: true,
				email: true
			},
			msg: {
				required: true,
				minlength:10
			},
			phoneNo: {
				required: true,
        number: true
			},
			messages: {
				fullname: "Please enter your fullname",
    		email: "Please enter a valid email address",
    		msg: "please enter only 10 digit",
    		phoneNo	: "please enter a valid phone number"
		}
	},
	submitHandler : function(form) {
    var form = $('#contact-form').serialize();
		$.ajax({
         type:'POST',
         url:"sendmail.php",
         data:form,
        success: function(result)
        {
        	$('#contact-form').html("<center><h3>Thank you for Contacting Us.</h3><br /><h5>We will Soon get in touch with you!</h5></center>");
        }
    	});
}
});
	
	$('.featured_slider').slick({
      dots: true,
      infinite: true,      
      speed: 800,
      arrows:false,      
      slidesToShow: 1,
      slide: 'div',
      autoplay: true,
      fade: true,
      autoplaySpeed: 5000,
      cssEase: 'linear'
    });

    function sticky_relocate() {
	    var window_top = $(window).scrollTop();
	    if($('#sticky-anchor').offset())
	    {
		    var div_top = $('#sticky-anchor').offset().top;
		    if (window_top > div_top) {
		        $('#sticky').addClass('stick');
		        $('#sticky-anchor').height($('#sticky').outerHeight());
		        $('#sticky').addClass('btn-danger');
		        $('#sticky').addClass('btn-xlarge ');
		        $('#sticky').text('Have A Query?');

		    } else {
		        $('#sticky').removeClass('stick');
		        $('#sticky').removeClass('btn-danger');
		        $('#sticky').removeClass('btn-xlarge ');
		        $('#sticky').text('Yes I am Interested');
		        $('#sticky-anchor').height(0);
		    }	
	    }
	}

	$('#submitForm').on('click',function(){
		var email = $('#email').val();
		var name = $('#name').val();
		var tel = $('#phone').val();
		var msg = $('#msg').val();
		 $.ajax({
         type:'POST',
         url: "sendmail.php",
         data:{email:email,name:name,phone:tel,msg:msg},
        success: function(result){
        	$('#submitForm').addClass('btn-success');	
        	$('#submitForm').val('Your mail was sent Successfully');
        }
		}); 
	});

	$("#countdown").countdown({
		date: "14 july 2017 00:00:00",
		format: "on"
	},

	function() {
		// callback function
	});

    $(function() {
    $(window).scroll(sticky_relocate);
    	sticky_relocate();
	});
});

